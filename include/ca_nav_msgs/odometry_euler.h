/*
 * odometry_euler.h
 *
 *  Created on: Mar 7, 2015
 *      Author: aeroscout
 */

#ifndef CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_ODOMETRY_EULER_H_
#define CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_ODOMETRY_EULER_H_

#include <nav_msgs/Odometry.h>
#include <Eigen/Dense>
#include <tf/transform_datatypes.h>

#include "geom_cast/geom_cast.hpp"

namespace ca {

struct OdometryEuler {
  double time;
  Eigen::Vector3d position;
  Eigen::Vector3d orientation;
  Eigen::Vector3d linear_velocity;
  Eigen::Vector3d angular_velocity;

  nav_msgs::Odometry GetMsg() {
    nav_msgs::Odometry odom;
    odom.header.stamp = ros::Time(time);
    odom.pose.pose.position = ca::point_cast<geometry_msgs::Point>(position);

    tf::Quaternion quat;
    quat.setRPY(orientation.x(), orientation.y(), orientation.z());
    odom.pose.pose.orientation = ca::rot_cast<geometry_msgs::Quaternion>(quat);

    odom.twist.twist.linear = ca::point_cast<geometry_msgs::Vector3>(linear_velocity);
    odom.twist.twist.angular = ca::point_cast<geometry_msgs::Vector3>(angular_velocity);
    return odom;
  }

  void SetMsg(const nav_msgs::Odometry &odom) {
    time = odom.header.stamp.toSec();
    position = ca::point_cast<Eigen::Vector3d>(odom.pose.pose.position);

    tf::Quaternion quat = ca::rot_cast<tf::Quaternion>(odom.pose.pose.orientation);
    tf::Matrix3x3(quat).getRPY(orientation[0], orientation[1], orientation[2]);

    linear_velocity = ca::point_cast<Eigen::Vector3d>(odom.twist.twist.linear);
    angular_velocity = ca::point_cast<Eigen::Vector3d>(odom.twist.twist.angular);
  }
};

}



#endif /* CA_NAV_MSGS_INCLUDE_CA_NAV_MSGS_ODOMETRY_EULER_H_ */
