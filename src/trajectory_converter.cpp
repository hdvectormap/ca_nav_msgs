#include "ca_nav_msgs/trajectory_converter.h"
#include "Eigen/Core"

ca_nav_msgs::PathXYZVPsi ca_nav_msgs::ConvertViewPointPath2XYZVPsi(ca_nav_msgs::PathXYZVViewPoint &ip_path){
    ca_nav_msgs::PathXYZVPsi op_path;
    ca_nav_msgs::XYZVPsi op_wp;
    ca_nav_msgs::XYZVViewPoint ip_wp;

    double param_time_trajectory = 0.1;
    for(size_t i=0; i< ip_path.waypoints.size();i++){
        ip_wp = ip_path.waypoints[i];
        op_wp.position = ip_wp.position;
        op_wp.vel = ip_wp.vel;

        if(ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_VIEWPOINT){
            double v1_x = ip_wp.viewpoint.x - ip_wp.position.x;
            double v1_y = ip_wp.viewpoint.y - ip_wp.position.y;
            op_wp.heading = std::atan2(v1_y,v1_x);
            op_path.waypoints.push_back(op_wp);
        }
        else if(i<(ip_path.waypoints.size()-2) && ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_FORWARD){
            ca_nav_msgs::XYZVViewPoint& ip_wp_next = ip_path.waypoints[i+1];
            double v1_x = -ip_wp.position.x + ip_wp_next.position.x;
            double v1_y = -ip_wp.position.y + ip_wp_next.position.y;
            op_wp.heading = std::atan2(v1_y,v1_x);
            op_path.waypoints.push_back(op_wp);
        }
        else if(ip_wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_FOV){
            if(i>0){
                ca_nav_msgs::XYZVViewPoint& ip_wp_prev = ip_path.waypoints[i-1];
                double vin_x = ip_wp.position.x - ip_wp_prev.position.x;
                double vin_y = ip_wp.position.y - ip_wp_prev.position.y;
                double vin_z = ip_wp.position.z - ip_wp_prev.position.z;
                Eigen::Vector3d pos(ip_wp.position.x,ip_wp.position.y,ip_wp.position.z);
                Eigen::Vector3d vec(vin_x,vin_y,vin_z); vec.normalize();
                double time_behind = vec.norm()/ip_wp.vel;
                for(double i=-time_behind;i<0; i=i+param_time_trajectory){
                    op_wp.position.x = pos.x() + i*vec.x();
                    op_wp.position.y = pos.y() + i*vec.y();
                    op_wp.position.z = pos.z() + i*vec.z();
                    op_wp.heading = ip_wp.min_heading + i*ip_wp.heading_rate*(ip_wp.max_heading - ip_wp.min_heading);
                    op_path.waypoints.push_back(op_wp);
                }
            }
            if(i<ip_path.waypoints.size()-2){
                ca_nav_msgs::XYZVViewPoint& ip_wp_next = ip_path.waypoints[i+1];
                double vout_x = -ip_wp.position.x + ip_wp_next.position.x;
                double vout_y = -ip_wp.position.y + ip_wp_next.position.y;
                double vout_z = -ip_wp.position.z + ip_wp_next.position.z;
                Eigen::Vector3d pos(ip_wp.position.x,ip_wp.position.y,ip_wp.position.z);
                Eigen::Vector3d vec(vout_x,vout_y,vout_z); vec.normalize();
                double time_ahead = vec.norm()/ip_wp.vel;
                for(double i=0;i<time_ahead; i=i+param_time_trajectory){
                    op_wp.position.x = pos.x() + i*vec.x();
                    op_wp.position.y = pos.y() + i*vec.y();
                    op_wp.position.z = pos.z() + i*vec.z();
                    op_wp.heading = ip_wp.min_heading + i*ip_wp.heading_rate*(ip_wp.max_heading - ip_wp.min_heading);
                    op_path.waypoints.push_back(op_wp);
                }
            }
        }

    }

    op_path.header = ip_path.header;

    return op_path;
}
